#include "util.h"
#include "base/PluginManager.h"
#include "CodeTrainerConfig.h"
#include <vector>

IFunction* GetPluginFunction(const std::string& iPluginType, const std::string& iPluginName)
{
    std::vector<std::string> functions;
    std::string pluginPath(INSTALL_PREFIX);

#ifdef _WIN32
    pluginPath += "/plugins/" + iPluginType + "/" + iPluginName + ".dll";
#else
    pluginPath += "/lib/codetrainer/plugins/" + iPluginType + "/lib" + iPluginName + ".so";
#endif

    functions.push_back(pluginPath);

    PluginManager* pPM = PluginManager::Get();
    pPM->ReleasePlugins();
    pPM->LoadFunctionPlugins(functions);

    const std::vector<FunctionPlugin*>& libFunctions = pPM->GetFunctionPlugins();
    if (libFunctions.size() != 1)
        return NULL;

    return libFunctions[0]->NewFunction();
}

