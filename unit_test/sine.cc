#include "gtest/gtest.h"
#include <string>
#include "base/Interfaces.h"
#include "util.h"

class TestSine: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
        Test::SetUp();

		pFunction = GetPluginFunction("functions","sine");
        if (!pFunction)
        {
            ASSERT_EQ(1, 0);
        }
	}

    virtual void TearDown()
    {
        Test::TearDown();
        delete pFunction;
    }

	IFunction* pFunction;
};

TEST_F(TestSine, TestSineOfZeroIsZero)
{
	EXPECT_EQ(0, pFunction->Evaluate(0));
}

TEST_F(TestSine, TestSineOfPiOverTwoIsOne)
{
    EXPECT_EQ(1, pFunction->Evaluate(3.141592653589793238463/2));
}
