#include <string>
#include "base/Interfaces.h"

IFunction* GetPluginFunction(const std::string& iPluginType, const std::string& iPluginName);
