#define _USE_MATH_DEFINES // M_PI defined for Visual Studio

#include "ExportApi.h"
#include "plugins/parametric_function/ParametricFunction.h"
#include <cmath>
#include <cstdio>

using namespace std;

EXTERN_C EXPORT_API int GetVersionLow(void)
{
    return CODE_TRAINER_PARAMETRIC_FUNCTION_PLUGIN_VERSION_LOW;
}

EXTERN_C EXPORT_API int GetVersionHigh(void)
{
    return CODE_TRAINER_PARAMETRIC_FUNCTION_PLUGIN_VERSION_HIGH;
}

EXTERN_C EXPORT_API const char *GetName(void)
{
    return "Harmonic oscillation";
}

EXTERN_C EXPORT_API const char *GetDescription(void)
{
    return "amplitute * sin(2*pi*frequency * T + delay)";
}

class Oscillation
{
public:
    Oscillation():
    m_amplitude(2.0),
    m_frequency(5.0),
    m_delay(0.0)
    {
    }

    double Evaluate(double param)
    {
        return m_amplitude * sin(2.0*M_PI*m_frequency * param + m_delay);
    }

    double m_amplitude; // 0
    double m_frequency; // 1
    double m_delay; // 2
};

EXTERN_C EXPORT_API Oscillation *CreateFunction()
{
    return new Oscillation();
}

EXTERN_C EXPORT_API void DestroyFunction(Oscillation *instance)
{
    delete instance;
}

EXTERN_C EXPORT_API int GetParameterCount(Oscillation *)
{
    return 3;
}

EXTERN_C EXPORT_API const char *GetParameterName(Oscillation*, int index)
{
    switch(index)
    {
        case 0:
            return "amplitude";
        case 1:
            return "frequency";
        case 2:
            return "delay";
        default:
            return NULL;
    }
}
EXTERN_C EXPORT_API bool GetParameterValue(Oscillation* instance, int index, double *value)
{
    if (!instance || !value)
    {
        return false;
    }

    bool bRet = true;
    switch(index)
    {
        case 0:
            *value = instance->m_amplitude;
            break;
        case 1:
            *value = instance->m_frequency;
            break;  
        case 2:
            *value = instance->m_delay;
            break;
        default:
            bRet = false;
    }

    return bRet;
}

EXTERN_C EXPORT_API bool SetParameterValue(Oscillation *instance, int index, double value)
{
    if (!instance)
        return false;

    switch(index)
    {
        case 0:
            instance->m_amplitude = value;
            break;
        case 1:
            instance->m_frequency = value;
            break;
        case 2:
            instance->m_delay = value;
            break;
		default:
			return false;
    }

	return true;
}

EXTERN_C EXPORT_API double Evaluate(Oscillation *instance, double param)
{
    return instance->Evaluate(param);
}
