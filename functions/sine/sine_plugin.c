#include "ExportApi.h"
#include "plugins/function/Function.h"
#include <math.h>

EXTERN_C EXPORT_API int GetVersionLow(void)
{
    return CODE_TRAINER_FUNCTION_PLUGIN_VERSION_LOW;
}

EXTERN_C EXPORT_API int GetVersionHigh(void)
{
    return CODE_TRAINER_FUNCTION_PLUGIN_VERSION_HIGH;
}

EXTERN_C EXPORT_API const char *GetName(void)
{
    return "sin";
}

EXTERN_C EXPORT_API const char *GetDescription(void)
{
    return "sine function";
}

EXTERN_C EXPORT_API double Function(double param)
{
    return sin(param);
}
