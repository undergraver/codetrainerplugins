#include "ExportApi.h"
#include "plugins/test/Test.h"

EXTERN_C EXPORT_API int GetVersionLow(void)
{
    return CODE_TRAINER_TEST_PLUGIN_VERSION_LOW;
}

EXTERN_C EXPORT_API int GetVersionHigh(void)
{
    return CODE_TRAINER_TEST_PLUGIN_VERSION_HIGH;
}

EXTERN_C EXPORT_API const char *GetName(void)
{
    return "Test for plugin loading";
}

EXTERN_C EXPORT_API const char *GetDescription(void)
{
    return "This is a test plugin!\nThis means the plugin loaded.";
}
