#include "ExportApi.h"
#include "plugins/test/Test.h"

EXTERN_C EXPORT_API int GetVersionLow(void)
{
    return CODE_TRAINER_TEST_PLUGIN_VERSION_LOW;
}

EXTERN_C EXPORT_API int GetVersionHigh(void)
{
    return CODE_TRAINER_TEST_PLUGIN_VERSION_HIGH;
}

EXTERN_C EXPORT_API const char *GetName(void)
{
    return "Hello test";
}

EXTERN_C EXPORT_API const char *GetDescription(void)
{
    return "Codetrainer says hello!";
}
